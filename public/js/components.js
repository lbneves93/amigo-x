var actions = {
    remover: [],
    convidar: [],
    desconvidar: []
};

$(document).ready(function () {
    /**
     * Configura o AJAX para realizar chamadas no WS
     * utilizando o token para o Backend.
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btn-search').click();
});

function searchUsers() {
    var search = $('#searchUser').val();
    var searchFor = $('#searchFor').val();
    var data = {
        "searchFor": searchFor,
        "search": search
    };

    $.ajax({
        url: baseUrl + '/search-users',
        type: 'POST',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        var html = '';
        resposta.users.forEach(function (user, key) {
            html += '<tr>\n                    <td>' + user.name + '</td>\n                    <td>' + user.email + '</td>\n                    <td><input id="' + user.id + '" name="users[]" value="' + user.id + '" type="checkbox"></td>\n                    </tr>';
        });
        var table = $('#tbody-users');
        table.html(html);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function searchUsersEdit(idGroup) {

    actions = {
        remover: [],
        convidar: [],
        desconvidar: []
    };

    var search = $('#searchUser').val();
    var searchFor = $('#searchFor').val();
    var data = {
        "searchFor": searchFor,
        "search": search,
        "idGroup": idGroup
    };

    $.ajax({
        url: baseUrl + '/search-users-edit',
        type: 'POST',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        var html = '';
        resposta.users.forEach(function (user, key) {
            html += '<tr>\n                    <td>' + user.name + '</td>\n                    <td>' + user.email + '</td>\n                    <td><button onclick="changeUsersTable(' + user.idUser + ',\'' + user.acao + '\')" type="button" id="' + user.id + '">' + user.acao + '</button></td>\n                    </tr>';
        });
        var table = $('#tbody-users');
        table.html(html);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function changeUsersTable(idUser, acao) {
    var route = '';
    var idGroup = $('#groupId').val();
    switch (acao) {

        case 'Remover':
            route = '/groups/delete-member';
            break;
        case 'Retirar Convite':
            route = '/invitations/uninvite';
            break;
        case 'Convidar':
            route = '/invitations/invite';
            break;

    }

    var data = {
        "idGroup": idGroup,
        "idUser": idUser
    };

    $.ajax({
        url: baseUrl + route,
        type: 'POST',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        toastr.success(resposta.mensagem);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function editGroup(idGroup) {

    var data = {
        "name": $('#groupName').val(),
        "idGroup": idGroup
    };

    $.ajax({
        url: baseUrl + '/groups/' + idGroup,
        type: 'PUT',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        toastr.success(resposta.mensagem);
        setTimeout(function () {
            location.reload();
        }, 3000);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function deleteGroup(idGroup, name) {

    var data = {
        "name": name,
        "idGroup": idGroup
    };

    $.ajax({
        url: baseUrl + '/groups/' + idGroup,
        type: 'DELETE',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        toastr.success(resposta.mensagem);
        setTimeout(function () {
            location.href = baseUrl + '/groups';
        }, 3000);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}
function declineInvitation(id) {
    $.ajax({
        url: baseUrl + '/invitations/' + id,
        type: 'DELETE'

    }).done(function (resposta) {
        toastr.success(resposta.success);
        setTimeout(function () {
            location.reload();
        }, 3000);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function acceptInvitation(id) {
    var data = { "id": id };
    $.ajax({
        url: baseUrl + '/accept-invitation',
        type: 'POST',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        toastr.success(resposta.success);
        setTimeout(function () {
            location.reload();
        }, 3000);
    }).fail(function (erro) {
        console.log(erro);
        toastr.error(erro.responseJSON.mensagem);
    });
}
$(document).ready(function () {

    getSorts('#searchFor');
});

function getSorts(option) {

    var idGroup = $(option).val();
    var data = { "idGroup": idGroup };
    $.ajax({
        url: baseUrl + '/get-sorts',
        type: 'POST',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        var divContent = '';
        var html = '';
        if (resposta.sorts.length <= 0) {
            divContent = '<div class="well text-center center col-md-12 col-xs-12">\n                    <h4>N\xE3o existe nenhum sorteio para este grupo!</h4>\n                    </div>';
            return $('#div-table-sorts').html(divContent);
        }

        divContent = '<table class="table table-striped">\n        <thead>\n            <tr>\n            <th scope="col">Evento</th>\n            <th scope="col">Status</th>\n            </tr>\n        </thead>\n        <tbody id="tbody-sorts">    \n        </tbody>\n        </table>';

        $('#div-table-sorts').html(divContent);

        resposta.sorts.forEach(function (sort, key) {
            if (sort.sorted == 1) {
                html += '<tr>\n                    <td>' + sort.event + '</td>\n                    <td><button type="button" onclick="sort(' + sort.id + ',true)" class="btn btn-info">Sortear Novamente</button></td>\n                    </tr>';
            } else {
                html += '<tr>\n                    <td>' + sort.event + '</td>\n                    <td><button type="button" onclick="sort(' + sort.id + ',false)" class="btn btn-primary">Sortear</button></td>\n                    </tr>';
            }
        });
        var table = $('#tbody-sorts');
        table.html(html);
    }).fail(function (erro) {
        console.log(erro.responseJSON.mensagem);
        toastr.error(erro.responseJSON.mensagem);
    });
}

function sort(idSort, sorted) {

    var data = {
        'idSort': idSort,
        'sorted': sorted
    };

    $.ajax({
        url: baseUrl + '/sorts/sort',
        type: 'POST',
        data: data,
        dataType: 'json'

    }).done(function (resposta) {
        toastr.success(resposta.mensagem);
        setTimeout(function () {
            location.reload();
        }, 3000);
        console.log(resposta);
    }).fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
        console.log(erro.responseJSON.mensagem);
    });
}

function showModal(friend) {
    $('#friend-name').html('<strong>Nome: </strong>' + friend.name);
    $('#friend-email').html('<strong>E-mail: </strong>' + friend.email);
}