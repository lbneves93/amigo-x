let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/**
* Arquivos JS da aplicação.
*/
arquivosJS = [ 
    './resources/assets/js/group.js',
    './resources/assets/js/invitation.js',
    './resources/assets/js/sort.js'
];

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


mix.setResourceRoot("/amigo-x/public/")
    .styles([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/toastr/build/toastr.min.css',
        'node_modules/font-awesome/css/font-awesome.min.css',
        'node_modules/magnific-popup/magnific-popup.css'

    ], 'public/css/lib.css')
    .scripts([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/toastr/build/toastr.min.js',
        'node_modules/jquery-easing/jquery.easing.min.js',
        'node_modules/scrollreveal/dist/scrollreveal.min.js'
        
    ], 'public/js/lib.js')
    .babel(arquivosJS, 'public/js/components.js')
    .copy([
        'node_modules/font-awesome/fonts/*',
        'node_modules/bootstrap/fonts/*',
        'resources/assets/fonts/*'
    ], 'public/fonts')

   

   
    