#!/bin/bash

app=amigo-x-app
url=http://ec2-18-231-14-119.sa-east-1.compute.amazonaws.com

echo subindo o container da aplicação
docker-compose up -d

echo copiando o arquivo de configuração.env
docker exec -it "$app" cp .env.example .env

echo instalando as dependências
docker exec -it "$app" composer install

echo gerando a chave
docker exec -it "$app" php artisan key:generate

echo realizando as migrations
docker exec -it "$app" php artisan migrate

echo realizando o seed da base de dados
docker exec -it "$app" php artisan db:seed

echo exibindo informações dos contâiners
docker ps -a

echo url para acessar a aplicação "$url"
