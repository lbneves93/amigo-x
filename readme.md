## Informações Gerais

Este projeto visa a criação de uma solução para sorteio de Amigo Secreto.
Permitindo que os usuários se cadastrem, criem grupos, mandem convites ao seus amigos, criem sorteios e realizem estes sorteio entre os membros do grupo, com uma tela inicial onde podem ser vistos os convites recebidos e o seu amigo secreto em cada sorteio.  

* Projeto desenvolvido utilizando [Framework Laravel](http://laravel.com/) 
* Download do projeto:
```
git clone https://lbneves93@bitbucket.org/lbneves93/amigo-x.git
```

* Copie o arquivo ".env.example" com o nome ".env" e configure as variaveis necessárias.
* Acesse a pasta do projeto via terminal.
* Crie uma chave ssh do projeto:
```
php artisan key:generate
```
* Instale as dependências do PHP utilzando o [Composer] (https://getcomposer.org/)
```
composer install
```
* Instale as dependências do Javascript utilzando o [NPM] (https://www.npmjs.com/)
```
npm install
```
* Compilando dependêcias JS,CSS,fonts e imagens para desenvolvimento
```
npm run dev
```
* Watcher para compilar as alterações nas dependencias em tempo real
```
npm run watch
```
caso não funcione, utilize:
```
npm run watch-poll
```
* Compilando dependêcias JS,CSS,fonts e imagens para produção
```
npm run prod
```
* Deploy com Docker em Servidores Linux.
* Acesse a pasta do projeto no servidor com docker e docker-compose instalados.

```
sh run.sh
```

## Funcionalidades

* Auto cadastro de usuários.
* Autenticação com email e senha.
* Visualização de convites.
* Visualização de resultados de sorteios.
* Cadastro,edição e deleção de grupos.
* Cadastro de sorteios.
* Enviar convites a usuários.
* Aceitar ou recusar convites.
* Realização de sorteios.



