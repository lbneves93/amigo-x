<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSortResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sort_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idSort')->unsigned();
            $table->foreign('idSort')
            ->references('id')->on('sorts')
            ->onDelete('cascade');
            $table->integer('idFirstFriend')->unsigned();
            $table->foreign('idFirstFriend')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->integer('idSecondFriend')->unsigned();            
            $table->foreign('idSecondFriend')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->timestamps();
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sort_results');
    }
}
