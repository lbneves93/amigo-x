<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::resource('groups', 'GroupController');

Route::resource('sorts', 'SortController');

Route::resource('invitations', 'InvitationController');

Route::post('accept-invitation', 'InvitationController@accept')->name('accept-invitation');

Route::post('search-users', 'UserController@search')->name('search-users');

Route::post('get-sorts', 'SortController@getSorts')->name('get-sorts');

Route::post('/sorts/sort', 'SortController@sort')->name('/sorts/sort');

Route::post('search-users-edit', 'UserController@usersToGroup')->name('search-users-edit');

Route::post('invitations/invite', 'InvitationController@invite')->name('inivite');

Route::post('invitations/uninvite', 'InvitationController@uninvite')->name('uninvite');

Route::post('groups/delete-member', 'GroupController@deleteMember')->name('delete-member');




/**
 * Redireciona todas as requisições em URL's não especificadas
 * para a página principal.
 */

Route::any('{all}', function () {
    return redirect("/");
})->where('all', '.*');

