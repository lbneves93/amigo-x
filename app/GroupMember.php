<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $fillable = [
        'id',
        'idMember',
        'idGroup'
    ];

    protected $table = 'group_members';
}
