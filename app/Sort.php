<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sort extends Model
{
    protected $fillable = [
        'id',
        'idGroup',
        'event',
        'sorted'
    ];

    protected $table = 'sorts';

    public function group(){

        return $this->belongsTo('App\Group', 'idGroup');        
    }
}
