<?php

namespace App;

use App\Group;

use Illuminate\Database\Eloquent\Model;

class SortResult extends Model
{
    protected $fillable = [
        'id',
        'idSort',
        'idFirstFriend',
        'idSecondFriend'
    ];

    protected $table = 'sort_results';

    public function sort()
    {
        return $this->belongsTo('App\Sort', 'idSort');
    }

    public function you()
    {
        return $this->belongsTo('App\User', 'idFirstFriend');    
    }

    public function friend()
    {
        return $this->belongsTo('App\User', 'idSecondFriend');            
    }
}
