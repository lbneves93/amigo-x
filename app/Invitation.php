<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $fillable = [
        'id',
        'idGuest',
        'idGroup'
    ];

    protected $table = 'invitations';

    public function guest()
    {
        return $this->belongsTo('App\User', 'idGuest');
    }

    public function group()
    {
        return $this->belongsTo('App\Group', 'idGroup');
    }
}
