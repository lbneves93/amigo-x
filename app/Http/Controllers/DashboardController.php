<?php

namespace App\Http\Controllers;

use App\Invitation;
use App\SortResult;
use Auth;

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invitations = Invitation::where('idGuest',Auth::user()->id)->get();
        $sortResults = SortResult::where('idFirstFriend',Auth::user()->id)->get(); 
        return view('dashboard', compact('invitations','sortResults'));
       
    }


}
