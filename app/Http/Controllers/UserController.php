<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Invitation;
use App\GroupMember;

class UserController extends Controller
{
    public function search(Request $request){
        try{
           if($request->searchFor == 'email'){
                $users = User::where('email','like','%'.$request->search.'%')
                            ->where('id','<>',Auth::user()->id)
                            ->get();
           }
           else{
                $users = User::where('name','like','%'.$request->search.'%')
                ->where('id','<>',Auth::user()->id)
                ->get();
           }

           return response()->json(['users'=>$users]);
            
        }
        catch(\Exception $e){
            return response()->json(['mensagem'=>$e], 404);
        }    
    }

    public function usersToGroup(Request $request){
        
        if($request->searchFor == 'email'){
            $users = User::where('email','like','%'.$request->search.'%')
                        ->where('id','<>',Auth::user()->id)
                        ->get();
        }
        else{
                $users = User::where('name','like','%'.$request->search.'%')
                ->where('id','<>',Auth::user()->id)
                ->get();
        }

        $usersFounded = [];

        if($users->isNotEmpty()){
            $users = $users->toArray();
            foreach($users as $user){

                // Verifica se o usuário ja está no grupo
                $userMember = GroupMember::where('idMember',$user['id'])
                                         ->where('idGroup',$request->idGroup)
                                         ->get();

                if($userMember->isNotEmpty()){
                    $usersFounded[]= [
                        'idUser'=>$user['id'],
                        'name'=> $user['name'],
                        'email'=>$user['email'],
                        'acao'=>'Remover'
                    ];
                    
                    continue;
                }

                //Verifica se o usuário já foi convidado
                $userInvitated = Invitation::where('idGuest',$user['id'])
                                           ->where('idGroup',$request->idGroup)
                                           ->get();
                                
                if($userInvitated->isNotEmpty()){
                    $usersFounded[]= [
                        'idUser'=>$user['id'],
                        'name'=> $user['name'],
                        'email'=>$user['email'],
                        'acao'=>'Retirar Convite'
                    ];
                    
                    continue;
                }
                
                // Usuário pode ser convidado.
                $usersFounded[]= [
                    'idUser'=>$user['id'],
                    'name'=> $user['name'],
                    'email'=>$user['email'],
                    'acao'=>'Convidar'
                ];
                
            }
        }

        return response()->json(['users'=>$usersFounded]);

    }
}
