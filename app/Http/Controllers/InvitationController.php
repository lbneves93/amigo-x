<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\GroupMember;
use Log;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Invitation::destroy($id);
            return response()->json(['success'=>'Convite recusado com sucesso!']);            
        }
        catch(\Exception $e){
            return response()->json(['mensagem'=>$e], 404);
        }    
    }

    public function accept(Request $request){
        try{
            $invitation = Invitation::find($request->id);
            
            $groupMember = new GroupMember();
            $groupMember->idMember = $invitation->idGuest;
            $groupMember->idGroup = $invitation->idGroup;
            $groupMember->save();

            Invitation::destroy($request->id);
            return response()->json(['success'=>'Bem vindo ao grupo: '.$invitation->group->name.'!']);
        }
        catch(\Exception $e){
            Log::error('Erro ao aceitar o convite: ',['exception'=>$e->getMessage()]);
            return response()->json(['mensagem'=>'Erro ao aceitar o convite!'], 404);
        }
    }

    public function invite(Request $request)
    {
        //Verifica se usuário já foi convidado.
        $userInvited = Invitation::where('idGuest',$request->idUser)
                               ->where('idGroup',$request->idGroup)
                               ->exists();
        if(!$userInvited){                       

            $invite = new Invitation();
            $invite->idGuest = $request->idUser;
            $invite->idGroup = $request->idGroup;
            $invite->save();

            return response()->json(['mensagem'=>'Convite enviado ao usuário!']);
        }

        return response()->json(['mensagem'=>'Convite já foi enviado ao usuário!']);
        
    }

    public function uninvite(Request $request)
    {
        $invite = Invitation::where('idGuest',$request->idUser)
                               ->where('idGroup',$request->idGroup)
                               ->delete();

        return response()->json(['mensagem'=>'Retirado convite do usuário!']);
    }
}
