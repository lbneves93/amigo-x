<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use Auth;
use App\Sort;
use App\SortResult;
use App\GroupMember;
use Log;


class SortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::where('idOwner',Auth::user()->id)->get();
        return view('sort.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::where('idOwner', Auth::user()->id)->get();
        return view('sort.create',compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sort = new Sort();
        $sort->idGroup = $request->sortGroup;
        $sort->event = $request->sortEvent;
        $sort->sorted = 0;
        $sort->save();

        return back()->with('success','Sorteio cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSorts(Request $request)
    {
        $sorts = Sort::where('idGroup',$request->idGroup)->get();
        return response()->json(['sorts'=>$sorts]); 
    }

    public function sort(Request $request){
        
        
        $sort = Sort::find($request->idSort);
        $members = GroupMember::where('idGroup',$sort->idGroup)->get();
        if($members->count() < 2){
            return response()->json(['mensagem'=>'Numero de participantes insuficiente para o sorteio.'],404);
        }
        
        if($request->sorted){
            $resultRemoveSort = $this->removeSortResults($request->idSort);
            if($resultRemoveSort){
                $resultSort = $this->doSort($request->idSort);
            }
            else{
                return response()->json(['mensagem'=>'Erro ao resortear o amigo secreto.'],500);    
            }    
        }
        else{
            $resultSort = $this->doSort($request->idSort);
        }

        if($resultSort){
            $sort->sorted = 1;
            $sort->save();
            return response()->json(['mensagem'=>'Amigo secreto sorteado com sucesso! Veja o seu amigo na tela inicial.']);
        
        }
        else{
            return response()->json(['mensagem'=>'Erro ao sortear o amigo secreto.'],500);
        }
    }

    private function doSort($idSort){

        $sort = Sort::find($idSort);
        $members = GroupMember::where('idGroup',$sort->idGroup)->get();
        if($members){
            $arrMembers = $members->toArray();
            $arrSortResults = [];
            
            foreach($arrMembers as $member){
                $friend = $members->where('idMember','<>',$member['idMember'])->random();
                $sortResult = new SortResult();
                $sortResult->idFirstFriend = $member['idMember'];
                $sortResult->idSecondFriend = $friend->idMember;
                $sortResult->idSort = $idSort;

                $arrSortResults[] = $sortResult;

                $members = $members->where('idMember','<>',$friend->idMember);    

            }

            try{
                foreach($arrSortResults as $sortResult){
                    $sortResult->save();
                }
                return true;
            }
            catch(\Exception $e){
                Log::error('Erro ao sortear o amigo secreto: ',['exception'=>$e->getMessage()]);
                return false;
            }
        }    
    }

    private function removeSortResults($idSort){
        try{
            SortResult::where('idSort',$idSort)->delete();
            return true;
        }
        catch(\Exception $e){
            Log::error('Erro ao resortear o amigo secreto: ',['exception'=>$e->getMessage()]);
            return false;
        }    
    }
}
