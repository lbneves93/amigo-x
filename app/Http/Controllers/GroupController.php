<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\Group;
use App\GroupMember;
use Auth;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::where('idOwner',Auth::user()->id)->get();
        return view('group.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $group = new Group();
        $group->name = $request->name;
        $group->idOwner = Auth::user()->id;
        $group->save();

        $member = new GroupMember();
        $member->idMember = Auth::user()->id;
        $member->idGroup = $group->id;
        $member->save();
        
        if(!empty($request->users)){
            foreach($request->users as $idUser){
                $invite = new Invitation();
                $invite->idGuest = $idUser;
                $invite->idGroup = $group->id;
                $invite->save();
                
            }
        }

        return back()->with('success','Grupo cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::find($id);
        return view('group.show',compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::find($id);
        if($group){
            $group->name = $request->name;
            $group->save();

            return response()->json(['mensagem'=>'Grupo alterado com sucesso!']);
        }

        return response()->json(['mensagem' => 'Grupo não encontrado!'],404);        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $group = Group::find($id);
        if($group){
            Group::where('id',$id)->delete();
            return response()->json(["mensagem"=>"Grupo deletado com sucesso!"]);            
        }
        
        return response()->json(["mensagem"=>"Grupo não encontrado!"],404);        
        
    }

    public function deleteMember(Request $request)
    {
        $member = GroupMember::where('idMember',$request->idUser)
                               ->where('idGroup',$request->idGroup)
                               ->delete();

        return response()->json(['mensagem'=>'Usuário retirado do grupo!']);
    }
}
