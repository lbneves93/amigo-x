@extends('layouts.app')

@section('content')
<div class="container">
    <h1><strong>Amigo X</strong></h1>
    <h3>Convide seus amigos e faça o seu sorteio de Amigo Secreto!</h3>
    
    <div class="row center-block col-md-12">
        @yield('card')
    </div>
</div>
<script
    type="text/javascript"
    src="{{asset('js/home.js')}}"
></script>
@endsection
