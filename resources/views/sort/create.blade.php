@extends('layouts.base')

@section('content')

    
    <div class="title-group col-md-12">
        <h2>Cadastrar Sorteio</h2>
    </div>
    @if(isset($groups) && $groups->isNotEmpty())
    <div class="col-md-12 col-xs-12 form-center">   
        <form action="{{route('sorts.store')}}" method="POST">
            @csrf
            <div class="row">
                    <div class="form-group col-xs-6 col-md-6">
                            <label for="searchFor">Grupo</label>
                            <select name="sortGroup" class="form-control" id="searchFor">
                                @foreach($groups as $group)
                                <option value="{{$group->id}}">{{$group->name}}</option>
                                @endforeach
                            </select>
                    </div>
                    
                <div class="form-group col-md-6">
                        <label for="sortEvent">Evento</label>
                        <input type="text" name="sortEvent" class="form-control" id="sortEvent" placeholder="Natal,Confraternização,etc...">        
                </div>
            </div>       
            <button type="submit" class="btn btn-primary btn-create-group">Cadastrar</button>
        </form>
    </div>
    @else
    <div class="well text-center center col-md-12 col-xs-12">
            <h3>Nenhum Grupo Cadastrado, crie um grupo para criar um sorteio!</h3>
    </div>
    @endif

@endsection
