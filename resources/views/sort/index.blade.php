@extends('layouts.base')

@section('content')

    <div class="title-group col-md-12">
        <h2>Sorteios</h2>
    </div>
    @if(isset($groups) && $groups->isNotEmpty())
    <div class="col-md-12 col-xs-12 form-center">   
                
                <div class="col-xs-12 col-md-12">
                        <label for="searchFor">Grupo</label>
                        <select onchange="getSorts(this)" class="form-control" id="searchFor">
                            @foreach($groups as $group)
                            <option value="{{$group->id}}">{{$group->name}}</option>
                            @endforeach                            
                        </select>
                </div>
                
                <div id="div-table-sorts">
                    
                </div>        
                <div class="col-md-12">
                        <a href="{{route('sorts.create')}}">
                            <i class="icon-group-new fas fa-plus-circle"></i>
                            <p class="p-group">Criar Sorteio</p>
                        </a>       
                </div>
        </div>
        @else
        <div class="well text-center center col-md-12 col-xs-12">
                <h3>Nenhum Grupo Cadastrado, crie um grupo, depois crie um sorteio!</h3>
        </div> 
        @endif


@endsection
