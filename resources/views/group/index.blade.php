@extends('layouts.base')

@section('content')

    <div class="title-group col-md-12">
        <h2>Grupos</h2>
    </div>
    @if(isset($groups) && $groups->isNotEmpty())
    @foreach($groups as $group)
    <div class="col-md-2">
        <a href="{{route('groups.show',['id'=>$group->id])}}">
            <i class="icon-group fas fa-users"></i>
            <p>{{$group->name}}</p>
        </a>       
    </div>
    @endforeach  
    @else
    <div class="well text-center center col-md-12 col-xs-12">
        <h3>Nenhum grupo cadastrado no momento!</h3>
    </div> 
    @endif
    <div class="col-md-12">
        <a href="{{route('groups.create')}}">
            <i class="icon-group-new fas fa-plus-circle"></i>
            <p class="p-group">Adicionar Grupo</p>
        </a>       
    </div> 


@endsection
