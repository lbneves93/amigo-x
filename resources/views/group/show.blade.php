@extends('layouts.base')

@section('content')

    
    <div class="title-group col-md-12">
        <h2>Editar Grupo</h2>
    </div>
    @if(isset($group))
    <div class="col-md-12 col-xs-12 form-center">   
        <form action="">
            @csrf
            <input type="hidden" id="groupId" value="{{$group->id}}">
            <div class="form-group col-xs-12 col-md-12">
                <label for="groupName">Nome</label>
                <input name="name" value="{{$group->name}}" type="text" class="form-control" id="groupName" aria-describedby="emailHelp" placeholder="Digite o nome do grupo">
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="searchFor">Pesquisar por</label>
                    <select class="form-control" id="searchFor">
                        <option value="email">E-mail</option>
                        <option value="nome">Nome</option>
                    </select>   
                </div>
                <div class="form-group col-md-6">
                        <label for="inviteSomeone">Convidar Participantes</label>
                        <input id="searchUser" type="text" class="form-control" id="inviteSomeone" placeholder="Pesquisar...">        
                </div>
                <div class="form-group col-md-2">
                    <label for="btn-search"></label>
                    <button type="button" id="btn-search" class="btn form-control" onclick="searchUsersEdit({{$group->id}})">Buscar</button>
                </div>
            </div>
            <div>
            <div>
                <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Ação</th>
                            </tr>
                        </thead>
                        <tbody id="tbody-users">
                            
                        </tbody>
                        </table>
            </div>
            <div class="row col-md-12">
                <div class="col-md-4">        
                    <a href="{{route('groups.index')}}" class="btn btn-info btn-create-group">Cancelar</a>            
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="editGroup({{$group->id}})" class="btn btn-primary btn-create-group">Salvar</button>
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="deleteGroup({{$group->id}})" class="btn btn-danger btn-create-group">Excluir</button>
                </div>    
            </div>
        </form>
    </div>
    @else
    <div class="well text-center center col-md-12 col-xs-12">
        <h3>Grupo não encontrado para a edição.</h3>
    </div>
    @endif
@endsection
