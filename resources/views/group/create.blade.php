@extends('layouts.base')

@section('content')

    
    <div class="title-group col-md-12">
        <h2>Cadastrar Grupo</h2>
    </div>
    <div class="col-md-12 col-xs-12 form-center">   
        <form action="{{route('groups.store')}}" method="POST">
            @csrf
            <div class="form-group col-xs-12 col-md-12">
                <label for="groupName">Nome</label>
                <input name="name" type="text" class="form-control" id="groupName" aria-describedby="emailHelp" placeholder="Digite o nome do grupo">
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="searchFor">Pesquisar por</label>
                    <select class="form-control" id="searchFor">
                        <option value="email">E-mail</option>
                        <option value="nome">Nome</option>
                    </select>   
                </div>
                <div class="form-group col-md-6">
                        <label for="inviteSomeone">Convidar Participantes</label>
                        <input id="searchUser" type="text" class="form-control" id="inviteSomeone" placeholder="Pesquisar...">
                            
                </div>
                <div class="form-group col-md-2">
                    <label for="btn-search"></label>
                    <button type="button" id="btn-search" class="btn form-control" onclick="searchUsers()">Buscar</button>
                </div>
            </div>
            <div>
            <div>
                <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Convidar</th>
                            </tr>
                        </thead>
                        <tbody id="tbody-users">
                            
                        </tbody>
                        </table>
            </div>        
            <button type="submit" class="btn btn-primary btn-create-group">Cadastrar</button>
        </form>
    </div>
    
@endsection
