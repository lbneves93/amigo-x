@extends('layouts.base')

@section('content')

@include('modals.sorted-friend')

<div class="col-md-6">
    <h3>Convites</h3>
    @if(isset($invitations) && $invitations->isNotEmpty())
    <table class="table table-striped">
            <thead>
            </thead>
            <tbody>
                @foreach($invitations as $invitation)
                <tr>
                    <td>{{'Você foi convidado para entrar no grupo: '.$invitation->group->name.'!'}}</td>  
                    <td><button onclick="acceptInvitation({{$invitation->id}})" class="btn btn-primary">Aceitar</button></td>
                    <td><button onclick="declineInvitation({{$invitation->id}})" class="btn btn-danger">Recusar</button></td>
                </tr>
                @endforeach
            </tbody>
    </table>
    @else
    <div class="well text-center center col-md-12 col-xs-12">
            <span>Você não tem nenhum convite novo.</span>
    </div>
    @endif

</div>
<div class="col-md-6">
        <h3>Sorteios</h3>
        @if(isset($sortResults) && $sortResults->isNotEmpty())
        <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Grupo</th>
                        <th scope="col">Evento</th>
                        <th scope="col">Amigo Secreto</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sortResults as $sortResult)
                    <tr>
                        <td>{{$sortResult->sort->group->name}}</td>
                        <td>{{$sortResult->sort->event}}</td>  
                        <td><button onclick="showModal({{$sortResult->friend}});" data-toggle="modal" data-target="#modal-sorted-friend" class="btn btn-primary">Ver Amigo</button></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              @else
              <div class="well text-center center col-md-12 col-xs-12">
                    <span>Nenhum grupo realizou sorteio.</span>
               </div>
              @endif
</div>    
@endsection
