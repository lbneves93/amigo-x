@extends('templates.home')

@section('card')
<div id="cadastro" class="card col-md-5 card-cadastre">
    <div class="card-header card-title">Cadastre-se</div>
    <form action="{{ route('register') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="row">
                <label for="name" class="col-md-5"><strong>Nome:</strong></label>
                <input autofocus name="name" required maxlength="50" placeholder="Insira o seu nome" id="name" type="text" class="col-md-5 form-control" >
            </div>
            <div class="row">
                <label for="email" class="col-md-5"><strong>E-mail:</strong></label>
                <input id="email-cadastro" type="email" class="col-md-5 form-control" name="email" value="{{ old('email') }}" required maxlength="70" placeholder="Insira o e-mail">
                
            </div>
            <div class="row">
                <label for="password" class="col-md-5"><strong>Senha:</strong></label>
                <input id="password-cadastro" type="password" class="col-md-5 form-control" name="password" required maxlength="255" placeholder="Insira a senha">
                
            </div>
            <div class="row">
                <label for="password-confirm" class="col-md-5"><strong>Repetir Senha:</strong></label>
                <input id="password-confirm" type="password" class="col-md-5 form-control" name="password_confirmation" required maxlength="255" placeholder="Repita a senha">
            </div>
            <div class="row">
                <button class="col-md-12 btn btn-primary" type="submit">Cadastrar</button>
            </div>    
        </div>
    </form>
</div>
@endsection
