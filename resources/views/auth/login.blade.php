@extends('templates.home')

@section('card')
<div id="login" class="card col-md-5 card-entre">
    <div class="card-header card-title">
        Entre
        <a class="esqueceu" href="">
            <strong>Esqueceu a Senha?</strong>
        </a>
    </div>

    <form action="{{ route('login') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="row form-group">
                <label for="email" class="col-md-5"><strong>E-mail:</strong></label>
                <input id="email" type="email" class="col-md-5 form-control" name="email" value="{{ old('email') }}" required autofocus maxlength="70" placeholder="Insira o e-mail">
            </div>
            <div class="row form-group">
                <label for="password" class="col-md-5"><strong>Senha:</strong></label>
                <input id="password" type="password" class="col-md-5 form-control" name="password" required maxlength="255" placeholder="Insira a senha">
                
            </div>
            <div class="row">
                <button class="col-md-12 btn-entrar btn btn-primary" type="submit">Entrar</button>
            </div>
            <div class="row">
                <p class="col-md-12 p-cadastre"><strong>Ainda não é cadastrado?</strong></p>
            </div>
            <div class="row">
                <a href="{{route('register')}}"class="link"><strong>Cadastre-se</strong></a>
            </div>
                
        </div>
    </form>
</div>
@endsection
