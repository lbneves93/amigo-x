<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Amigo X</title>

    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <link
        href="{{asset('css/app.css')}}"
        rel="stylesheet"
        type="text/css"
    />

    <link
        href="{{asset('css/lib.css')}}"
        rel="stylesheet"
        type="text/css"
    />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/creative.css')}}" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="{{route('dashboard')}}">Amigo X</a>
      <p class="hello-user navbar-brand js-scroll-trigger">Bem vindo, {{Auth::user()->name}}</p>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{route('dashboard')}}">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{route('groups.index')}}">Grupos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{route('sorts.index')}}">Sorteios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#">Editar Senha</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{route('logout')}}">Sair</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">

        <div class="container my-auto">
                <div class="row">
                    @yield('content')
                </div>
        </div>
        
    </header>

    <script type="text/javascript">
      var baseUrl = "{{ url('/') }}"
    </script>
    <script
        type="text/javascript"
        src="{{asset('js/lib.js')}}"
    ></script>
    <script
        type="text/javascript"
        src="{{asset('js/app.js')}}"
    ></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('js/creative.js')}}"></script>
    
    <script src="{{asset('js/components.js')}}"></script>
    

    @include('messages.errors')
    @include('messages.success')

    

  </body>

</html>
