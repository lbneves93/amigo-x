<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'As senhas precisam conter ao menos seis caracteres e ser igual a confirmação.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'Enviamos por e-mail o link para resetar sua senha!',
    'token' => 'O token para essa troca de senha é invalido.',
    'user' => "Não conseguimos encontrar um usuário com esse endereço de e-mail.",

];
