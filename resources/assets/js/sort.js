$(document).ready(function(){

    getSorts('#searchFor');
});

function getSorts(option){
    
    let idGroup = $(option).val();
    let data = {"idGroup":idGroup};
    $.ajax({
        url: baseUrl + '/get-sorts',
        type: 'POST',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        let divContent = '';
        let html = '';
        if(resposta.sorts.length <= 0){
            divContent = `<div class="well text-center center col-md-12 col-xs-12">
                    <h4>Não existe nenhum sorteio para este grupo!</h4>
                    </div>`;
            return $('#div-table-sorts').html(divContent);
        }
        
        divContent = `<table class="table table-striped">
        <thead>
            <tr>
            <th scope="col">Evento</th>
            <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody id="tbody-sorts">    
        </tbody>
        </table>`;

        $('#div-table-sorts').html(divContent);

        resposta.sorts.forEach(function(sort,key){
            if(sort.sorted == 1){
                html+= `<tr>
                    <td>${sort.event}</td>
                    <td><button type="button" onclick="sort(${sort.id},true)" class="btn btn-info">Sortear Novamente</button></td>
                    </tr>`
                
            }
            else{
                html+= `<tr>
                    <td>${sort.event}</td>
                    <td><button type="button" onclick="sort(${sort.id},false)" class="btn btn-primary">Sortear</button></td>
                    </tr>`
            }
            
        });
        let table = $('#tbody-sorts');
        table.html(html);
    })
    .fail(function (erro) {
        console.log(erro.responseJSON.mensagem);
        toastr.error(erro.responseJSON.mensagem);
    });
}

function sort(idSort,sorted){

    let data = {
        'idSort':idSort,
        'sorted':sorted
    };

    
    $.ajax({
        url: baseUrl + '/sorts/sort',
        type: 'POST',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        toastr.success(resposta.mensagem);
        setTimeout(function(){
            location.reload();
        },3000);
        console.log(resposta);
        
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
        console.log(erro.responseJSON.mensagem);
    });

}

function showModal(friend){
    $('#friend-name').html('<strong>Nome: </strong>'+friend.name);
    $('#friend-email').html('<strong>E-mail: </strong>'+friend.email);
}