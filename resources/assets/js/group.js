var actions = {
    remover:[],
    convidar:[],
    desconvidar:[]
};

$(document).ready(function () {
    /**
     * Configura o AJAX para realizar chamadas no WS
     * utilizando o token para o Backend.
     */
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#btn-search').click();
});


function searchUsers(){
    let search = $('#searchUser').val();
    let searchFor = $('#searchFor').val(); 
    let data = {
        "searchFor":searchFor,
        "search":search
    };

    $.ajax({
        url: baseUrl + '/search-users',
        type: 'POST',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        let html = '';
        resposta.users.forEach(function(user,key){
            html+= `<tr>
                    <td>${user.name}</td>
                    <td>${user.email}</td>
                    <td><input id="${user.id}" name="users[]" value="${user.id}" type="checkbox"></td>
                    </tr>`
        });
        let table = $('#tbody-users');
        table.html(html);
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}


function searchUsersEdit(idGroup){
    
    actions = {
        remover:[],
        convidar:[],
        desconvidar:[]
    };

    let search = $('#searchUser').val();
    let searchFor = $('#searchFor').val(); 
    let data = {
        "searchFor":searchFor,
        "search":search,
        "idGroup":idGroup
    };

    $.ajax({
        url: baseUrl + '/search-users-edit',
        type: 'POST',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        let html = '';
        resposta.users.forEach(function(user,key){
            html+= `<tr>
                    <td>${user.name}</td>
                    <td>${user.email}</td>
                    <td><button onclick="changeUsersTable(${user.idUser},'${user.acao}')" type="button" id="${user.id}">${user.acao}</button></td>
                    </tr>`
        });
        let table = $('#tbody-users');
        table.html(html);
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function changeUsersTable(idUser,acao){
    let route = '';
    let idGroup = $('#groupId').val();
    switch(acao){
        
        case 'Remover':
            route = '/groups/delete-member';
            break;
        case 'Retirar Convite':
            route = '/invitations/uninvite';
            break;
        case 'Convidar':
            route = '/invitations/invite';
            break;

    }

    let data = {
        "idGroup": idGroup,
        "idUser": idUser
    };

    $.ajax({
        url: baseUrl + route,
        type: 'POST',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        toastr.success(resposta.mensagem);        
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });


}

function editGroup(idGroup){
    
    let data = {
        "name":$('#groupName').val(),
        "idGroup":idGroup
    };

    $.ajax({
        url: baseUrl + '/groups/'+idGroup,
        type: 'PUT',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        toastr.success(resposta.mensagem);
        setTimeout(function(){
            location.reload();
        },3000);
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function deleteGroup(idGroup,name){
    
    let data = {
        "name":name,
        "idGroup":idGroup
    };

    $.ajax({
        url: baseUrl + '/groups/'+idGroup,
        type: 'DELETE',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        toastr.success(resposta.mensagem);
        setTimeout(function(){
            location.href = baseUrl + '/groups';
        },3000);
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}