function declineInvitation(id){
    $.ajax({
        url: baseUrl + '/invitations/'+id,
        type: 'DELETE'
        
    })
    .done(function (resposta) {
        toastr.success(resposta.success);
        setTimeout(function(){
            location.reload();
        },3000);
    })
    .fail(function (erro) {
        toastr.error(erro.responseJSON.mensagem);
    });
}

function acceptInvitation(id){
    let data = {"id":id};
    $.ajax({
        url: baseUrl + '/accept-invitation',
        type: 'POST',
        data: data,
        dataType: 'json'
        
    })
    .done(function (resposta) {
        toastr.success(resposta.success);
        setTimeout(function(){
            location.reload();
        },3000);
    })
    .fail(function (erro) {
        console.log(erro);
        toastr.error(erro.responseJSON.mensagem);
    });
}